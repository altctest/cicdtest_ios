//
//  ViewController.swift
//  cicdtests
//
//  Created by Michał Kos on 17/10/2018.
//  Copyright © 2018 mkos. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var environmentLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        #if QA
            environmentLabel.text = "QA"
        #else
            environmentLabel.text = "DEV"
        #endif
    }
}
